const express = require('express')
const bodyParser = require('body-parser'); 

const app = express()

app.use(bodyParser.json());
const port = process.env.PORT || 3000

app.use(express.json());

let studenthouses = []

app.get('/', function (req, res) {
  res.send('Hello World!')
})

//UC-103
app.get('/api/info/', (req, res) => res.status(200).send({
    author: {
      name: "Jamie Surowiec", 
      studentnumber: 2123268
    },
    description: "App om samen maaltijden te eten.",
    sonarCubeURL: null
})); 

//UC-201
app.post('/api/studenthome', (req, res) => {
  const studenthouse = []
  const homeId = {homeId: studenthouses.length}
  studenthouse.push(homeId)
  studenthouse.push(req.body)
  studenthouses.push(studenthouse)
  res.status(200).json({
    result: studenthouse
  })
})

//UC-202
app.get('/api/studenthome',  (req, res) => {
  const name = req.query.name;
  const city = req.query.city;

  const houses = []

  //werkt alleen maar met 1 zoekterm
  for (let i = 0; i < studenthouses.length; i++){
    if(studenthouses[i].name === name || studenthouses[i].city === city){
      houses.push(studenthouses[i])
    }
  }
  
  res.status(200).json({
    result: houses
  })
});

//UC-203
app.get('/api/studenthome/:homeId', (req, res) => {
  const homeId = req.params.homeId;
  const studenthouse = studenthouses[homeId]
  if(studenthouse == null){
    res.status(404).json({
      result: 'Studenthome with given ID was not found'
    })
  }
  res.status(200).json({
    result: studenthouse
  })
});

//UC-204
app.put('/api/studenthome/:homeId', (req, res) => {
  const homeId = req.params.homeId;
  
  if(studenthouses[homeId] == null){
    res.status(404).json({
      result: 'Studenthome with given ID was not found'
    })
  }
  //na de update kan de studenthome alleen met id benanderd worden 
  studenthouses[homeId] = req.body;

  res.status(200).json({
    result: studenthouses[homeId]
  })
})

//UC-205
app.delete('/api/studenthome/:homeId/', (req, res) => {
  const homeId = req.params.homeId;
  const studenthouse = studenthouses[homeId]

  if(studenthouse == null){
    res.status(404).json({
      result: 'Studenthome with given ID was not found'
    })
  }
  
  studenthouses.splice(homeId, 1);
  
  res.status(200).json({
    result: studenthouse
  })
})

//UC-206
app.put('/api/studenthome/:homeId/user', (req, res) => {
  const homeId = req.params.homeId;
  let studenthouse = studenthouses[homeId]
  
  const userId = []
  userId.push(req.body)
  studenthouses[homeId].push(userId);

  res.status(200).json({
    result: studenthouse
  })
})

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))